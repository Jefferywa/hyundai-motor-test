module.exports = {
    printWidth: 100,
    singleQuote: true,
    trailingComma: 'es5',
    arrowParens: 'always',
    bracketSpacing: false,
    jsxBracketSameLine: false,
    tabWidth: 4,
    useTabs: true,
    semi: true,
};