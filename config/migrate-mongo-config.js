module.exports = {
	mongodb: {
		url: 'mongodb://localhost:27017',
		databaseName: 'hyundai_motor_db',
		options: {
			useNewUrlParser: true,
			useUnifiedTopology: true,
			//   connectTimeoutMS: 3600000, // increase connection timeout to 1 hour
			//   socketTimeoutMS: 3600000, // increase socket timeout to 1 hour
		},
	},
	migrationsDir: 'migrations',
	changelogCollectionName: 'migrations',
};
