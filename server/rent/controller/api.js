const router = require('express').Router();
const asyncWrap = require('express-async-wrap');

const {rentStartParams} = require('./validators');

const {rentStartMapper} = require('./mapper');

const {rentStartDependency} = require('./ioc');

router.post(
	'/start/:carId',
	rentStartParams,
	rentStartMapper,
	rentStartDependency,
	asyncWrap(async (req, res, next) => {
		await res.locals.appRentStartService.execute(res.locals.appParams);

		res.result = 'OK';

		next();
	})
);

module.exports = router;
