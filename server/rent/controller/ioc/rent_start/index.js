const Container = require('true-ioc');

const register = require('./register');

const rentStartContainer = register();

/**
 * @param req
 * @param log
 * @param mongo
 * @returns {Container}
 */
exports.rentStartRegister = (req, mongo, log) => {
	const perRequestContainer = new Container(rentStartContainer);

	perRequestContainer.registerValue('req', req);
	perRequestContainer.registerValue('mongo', mongo);
	perRequestContainer.registerValue('log', log);

	return perRequestContainer;
};
