const Container = require('true-ioc');

const AppRentStartService = require('./../../../application/app_rent_start_service');

const RentRepository = require('./../../../infrastructure/repository/rent_repository');
const CarRepository = require('./../../../infrastructure/repository/car_repository');

const RentService = require('./../../../infrastructure/service/rent_service');

module.exports = () => {
	const container = new Container();

	// App service
	container.registerClass('appRentStartService', AppRentStartService);

	// Repository
	container.registerClass('carRepository', CarRepository);
	container.registerClass('rentRepository', RentRepository);

	// Service
	container.registerClass('rentService', RentService);

	return container;
};
