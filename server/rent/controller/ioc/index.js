const config = require('config');

const MongoClientFactory = require('./../../../storage/mongo/mongo_client_factory');

const {rentStartRegister} = require('./rent_start');

exports.rentStartDependency = async (req, res, next) => {
	const {log} = req;

	const mongo = await MongoClientFactory.create(config, log);

	const container = rentStartRegister(req, mongo, log);
	const appRentStartService = container.getInstance('appRentStartService');

	res.locals = {
		...res.locals,
		appRentStartService,
	};

	next();
};
