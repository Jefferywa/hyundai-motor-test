const Car = require('../../domains/car');
const Diapason = require('../../domains/rent/diapason');

exports.rentStartMapper = (req, res, next) => {
	const {carId, diapason: diapasonData} = res.locals.params;

	const diapason = new Diapason(diapasonData.start, diapasonData.end);

	res.locals.appParams = {
		carId: carId,
		diapason: diapason,
	};

	next();
};
