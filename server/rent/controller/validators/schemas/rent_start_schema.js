const Joi = require('joi');

const rentStartSchema = Joi.object({
	carId: Joi.number().required(),
	diapason: Joi.object({
		start: Joi.date()
			.min('now')
			.required(),
		end: Joi.date().required(),
	}).required(),
});

exports.validate = async (params) => {
	return Joi.validate(params, rentStartSchema);
};
