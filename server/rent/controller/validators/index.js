const asyncWrap = require('express-async-wrap');
const rentStartSchema = require('./schemas/rent_start_schema');

exports.rentStartParams = asyncWrap(async (req, res, next) => {
	const params = {
		...req.body,
		...req.params,
	};

	res.locals.params = await rentStartSchema.validate(params);

	next();
});
