const {StatusError} = require('../../../entities/errors');

const Rent = require('../../../domains/rent');

class AppRentStartService {
	/**
	 * @param {Rent.RentService} rentService
	 * @param {Rent.CarRepository} carRepository
	 * @param {Rent.RentRepository} rentRepository
	 * @param {Logger} log
	 */
	constructor(rentService, carRepository, rentRepository, log) {
		this._rentService = rentService;
		this._carRepository = carRepository;
		this._rentRepository = rentRepository;

		this._log = log;
	}

	/**
	 * @param {Number} carId
	 * @param {Diapason} diapason
	 * @returns {Promise<void>}
	 */
	async execute({carId, diapason}) {
		this._log.json({stringData: {carId, diapason}}, 'TRY_CREATE_RENT');

		const car = await this._carRepository.get(carId);
		if (!car) {
			throw new StatusError('Car not found.');
		}

		const isNotAvailable = await this._rentService.checkCarAvailable(car, diapason);
		if (isNotAvailable) {
			this._log.warn('CAR_NOT_AVAILABLE');

			throw new StatusError('Car not available.');
		}

		const rent = Rent.create(car, diapason);

		await this._rentRepository.store(rent);
	}
}

module.exports = AppRentStartService;
