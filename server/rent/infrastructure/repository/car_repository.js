const collectionNames = require('../../../storage/mongo/collection_names');

const Car = require('../../../domains/car');

/**
 * @name Rent.CarRepository
 */
class CarRepository {
	/**
	 * @param {Db} mongo
	 * @param {Logger} log
	 */
	constructor(mongo, log) {
		this._mongo = mongo;
		this._log = log;
	}

	/**
	 * @param {Number} carId
	 * @returns {Promise<Car|null>}
	 */
	async get(carId) {
		this._log.json({stringData: {carId}}, 'TRY_GET_CAR');

		const car = await this._mongo.collection(collectionNames.CARS).findOne({
			id: carId,
		});

		if (!car) {
			return null;
		}

		return Car.fromSnapshot(car);
	}
}

module.exports = CarRepository;
