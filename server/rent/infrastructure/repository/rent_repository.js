const collectionNames = require('../../../storage/mongo/collection_names');

const Rent = require('../../../domains/rent');

/**
 * @name Rent.RentRepository
 */
class RentRepository {
	/**
	 * @param {Db} mongo
	 * @param {Logger} log
	 */
	constructor(mongo, log) {
		this._mongo = mongo;
		this._log = log;
	}

	/**
	 * @param {Rent} rent
	 * @returns {Promise<void>}
	 */
	async store(rent) {
		this._log.json({stringData: {rent}}, 'TRY_STORE_RENT');

		await this._mongo.collection(collectionNames.RENTS).insertOne(rent);
	}

	/**
	 * @param {Car} car
	 * @param {Diapason} diapason
	 * @returns {Promise<Rent|null>}
	 */
	async get(car, diapason) {
		this._log.json({stringData: {car, diapason}}, 'TRY_GET_RENT');

		const {id: carId} = car.toJSON();
		const {start: rentStartTime, end: rentEndTime} = diapason.toJSON();

		const [rent] = await this._mongo
			.collection(collectionNames.RENTS)
			.find({
				'car.id': carId,
				'diapason.start': {
					$gte: rentStartTime,
					$lt: rentEndTime,
				},
			})
			.sort({
				createdAt: -1,
			})
			.limit(1)
			.toArray();

		if (!rent) {
			return null;
		}

		return Rent.fromSnapshot(rent);
	}
}

module.exports = RentRepository;
