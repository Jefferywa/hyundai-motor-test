/**
 * @name Rent.RentService
 */
class RentService {
	/**
	 * @param {Rent.RentRepository} rentRepository
	 * @param {Logger} log
	 */
	constructor(rentRepository, log) {
		this._rentRepository = rentRepository;
		this._log = log;
	}

	/**
	 * @param {Car} car
	 * @param {Diapason} diapason
	 * @returns {Promise<Boolean>}
	 */
	async checkCarAvailable(car, diapason) {
		const lastRent = await this._rentRepository.get(car, diapason);

		if (!lastRent) {
			return false;
		}

		return true;
	}
}

module.exports = RentService;
