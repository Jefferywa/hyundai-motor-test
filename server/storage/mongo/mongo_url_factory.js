class MongoUrlFactory {
	/**
	 * @param {Object} config
	 * @returns {String}
	 */
	static create(config) {
		const {user, password, hosts, port, database, readPreference, replicaSet} = config;
		const options = {replicaSet, readPreference};
		const schema = 'mongodb://';

		const credentials = [user, password].filter((value) => value).join(':');

		const hostsString = hosts.map((host) => [host, port].join(':')).join(',');

		const optionsStr = Object.keys(options)
			.filter((key) => options[key])
			.map((key) => key + '=' + options[key])
			.join('&');

		let url = schema;
		if (credentials) {
			url += credentials + '@';
		}

		url += hostsString + '/';
		url += database + '?';

		if (optionsStr) {
			url += optionsStr;
		}

		return url;
	}
}

module.exports = MongoUrlFactory;
