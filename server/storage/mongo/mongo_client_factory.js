const MongoClient = require('mongodb').MongoClient;
const {EventEmitter} = require('events');

const MongoUrlFactory = require('./mongo_url_factory');

let promise = null;
class MongoClientFactory {
	/**
	 * @param config
	 * @param {Logger} log
	 * @returns {Promise<Db>}
	 */
	static async create(config, log) {
		const url = MongoUrlFactory.create(config.database.mongo);
		const event = new EventEmitter();

		if (promise) {
			return promise;
		}

		promise = new Promise((resolve) => {
			event.once('connected', resolve);
		});

		log.json({secureMongodbUri: url}, 'TRY_CREATE_MONGO_CLIENT');

		try {
			const client = await MongoClient.connect(url, {
				useUnifiedTopology: true,
			});
			const db = client.db(config.database.mongo.database);

			client.on('close', (data) => {
				log.warn('MONGO_CLIENT_CLOSE');
				log.json({stringData: {data}}, 'MONGO_CLIENT_CLOSE');

				promise = null;
			});

			event.emit('connected', db);

			return promise;
		} catch (err) {
			log.error({err}, 'CREATE_MONGO_CLIENT_ERROR');

			promise = null;
			return Promise.reject(err);
		}
	}
}

module.exports = MongoClientFactory;
