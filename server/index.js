const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');

const config = require('config');

const {Logger: LoggerFactory, Timer} = require('./../logger');
const loggerOptions = require('./../logger_options');

const {ExtendableError} = require('../server/entities/errors');

const APP_PORT = config.server.port;
const APP_MODE = config.server.mode;

const DEFAULT_ERROR_CODE = 500;
const HANDLED_ERROR_CODE = 400;

const DEFAULT_ERROR_TITLE = 'Что-то пошло нет так';
const DEFAULT_ERROR_MESSAGE = 'Unhandled error';

const HANDLED_ERROR_LIST = ['StatusError', 'ValidationError'];

const app = express();
const logger = LoggerFactory.create(loggerOptions);
logger.level(loggerOptions.level);

app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(logger.middleware);

app.use((req, res, next) => {
	req.timeStart = process.hrtime();
	next();
});

app.use('/rent', require('./rent/controller').api);
app.use('/calendar', require('./calendar/controller').api);

app.use((req, res, next) => {
	const shouldProcessError = !res.result && res.result !== null;

	if (shouldProcessError) {
		return next();
	}

	const time = Timer.hrtimeToMs(process.hrtime(req.timeStart));
	res.json({
		error: null,
		result: res.result instanceof ExtendableError ? res.result.message : res.result,
		meta: {
			time,
		},
	});
});

app.use((err, req, res, next) => {
	const time = Timer.hrtimeToMs(process.hrtime(req.timeStart));

	const error = {
		code: err.message.code || DEFAULT_ERROR_CODE,
	};

	if (HANDLED_ERROR_LIST.includes(err.name)) {
		error.code = HANDLED_ERROR_CODE;
		error.name = err.name;
		error.message = err.message.msg || err.message || DEFAULT_ERROR_MESSAGE;
		error.title = err.message.title || DEFAULT_ERROR_TITLE;

		if (typeof error.message === 'object') {
			error.message = error.message.msg || JSON.stringify(error.message);
		}
	} else {
		err.message = JSON.stringify(err.message);
		if (req.log) {
			req.log.error({err}, 'UNHANDLED_ERROR_CAUGHT');
		} else {
			logger.error({err}, 'UNHANDLED_ERROR_CAUGHT');
		}
	}

	res.json({
		error: error,
		result: null,
		meta: {
			time: time,
		},
	});

	next();
});

app.listen(APP_PORT, () => {
	logger.json({stringData: {port: APP_PORT, mode: APP_MODE}}, 'SERVER_LISTEN_PORT');
});

process.on('unhandledRejection', (reason) => {
	logger.error({secureError: {err: reason}}, 'UNHANDLED_REJECTION');
});
