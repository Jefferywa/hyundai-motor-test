class ExtendableError extends Error {
	constructor(message) {
		super(message);
		this.name = this.constructor.name;
		this.message = message;
		Error.captureStackTrace(this, this.constructor.name);
	}
}

class HttpError extends ExtendableError {
	constructor(message) {
		super(message);
		this.name = 'HttpError';
	}
}

class InvalidJsonError extends ExtendableError {
	constructor(message) {
		super(message);
		this.name = 'InvalidJsonError';
	}
}

class StatusError extends ExtendableError {
	constructor(message) {
		super(message);
		this.name = 'StatusError';
	}
}

class ValidationError extends ExtendableError {
	constructor(message) {
		super(message);
		this.name = 'ValidationError';
	}
}

class ExpiredError extends ExtendableError {
	constructor(message) {
		super(message);
		this.name = 'ExpiredError';
	}
}

class NotImplementError extends ExtendableError {
	constructor(message) {
		super(message);
		this.name = 'NotImplementError';
	}
}

class TimezoneError extends ExtendableError {
	constructor(message) {
		super(message);
		this.name = 'TimezoneError';
	}
}

class ClientError extends ExtendableError {
	constructor(message, url, statusCode) {
		super(message);
		this.name = 'ClientError';
		this.url = url;
		this.statusCode = statusCode;
	}
}

class ServerError extends ExtendableError {
	constructor(message, url, statusCode) {
		super(message);
		this.name = 'ServerError';
		this.url = url;
		this.statusCode = statusCode;
	}
}

class ConnectionError extends ExtendableError {
	constructor(message) {
		super(message);
		this.name = 'ConnectionError';
	}
}

module.exports = {
	HttpError,
	InvalidJsonError,
	StatusError,
	ValidationError,
	ExpiredError,
	ExtendableError,
	NotImplementError,
	TimezoneError,
	ClientError,
	ServerError,
	ConnectionError,
};
