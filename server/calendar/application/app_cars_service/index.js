const Mapper = require('./mapper');

class AppCarsService {
	/**
	 * @param {Calendar.CarRepository} carRepository
	 * @param {Logger} log
	 */
	constructor(carRepository, log) {
		this._carRepository = carRepository;
		this._log = log;
	}

	/**
	 * @returns {Promise<Object>}
	 */
	async execute() {
		this._log.info('TRY_GET_CARS');

		const carList = await this._carRepository.getAll();
		return Mapper.mapToDto(carList);
	}
}

module.exports = AppCarsService;
