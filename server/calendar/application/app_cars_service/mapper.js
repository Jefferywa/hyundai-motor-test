class Mapper {
	/**
	 * @param {Car[]} carList
	 * @returns {Object}
	 */
	static mapToDto(carList) {
		const dtoCarList = carList.map((car) => {
			const {id, model, license} = car.toJSON();

			return {
				id: id,
				model: model,
				license: license,
			};
		});

		return {
			cars: dtoCarList,
		};
	}
}

module.exports = Mapper;
