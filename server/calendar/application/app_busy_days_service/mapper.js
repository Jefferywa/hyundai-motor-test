class Mapper {
	/**
	 * @param {Rent[]} rentList
	 * @returns {Object}
	 */
	static mapToDto(rentList) {
		const busyDaysList = rentList.map((rent) => {
			const {car, diapason} = rent.toJSON();
			const {id} = car.toJSON();
			const {start, end} = diapason.toJSON();

			return {
				carId: id,
				rentStartDay: start,
				rentEndDay: end,
			};
		});

		return {
			busyDays: busyDaysList,
		};
	}
}

module.exports = Mapper;
