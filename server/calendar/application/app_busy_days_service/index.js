const Mapper = require('./mapper');
class AppBusyDaysService {
	/**
	 * @param {Calendar.RentRepository} rentRepository
	 * @param {Logger} log
	 */
	constructor(rentRepository, log) {
		this._rentRepository = rentRepository;
		this._log = log;
	}

	/**
	 * @returns {Promise<Object>}
	 */
	async execute() {
		this._log.info('TRY_GET_BUSY_DAYS');

		const rentList = await this._rentRepository.getAllActual();
		return Mapper.mapToDto(rentList);
	}
}

module.exports = AppBusyDaysService;
