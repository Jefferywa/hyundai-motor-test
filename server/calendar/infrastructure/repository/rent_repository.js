const collectionNames = require('../../../storage/mongo/collection_names');

const Rent = require('../../../domains/rent');

/**
 * @name Calendar.RentRepository
 */
class RentRepository {
	/**
	 * @param {Db} mongo
	 * @param {Logger} log
	 */
	constructor(mongo, log) {
		this._mongo = mongo;
		this._log = log;
	}

	/**
	 * @returns {Promise<Rent[]>}
	 */
	async getAllActual() {
		this._log.info('TRY_GET_ALL_ACTUAL_RENTS');

		const rentList = await this._mongo
			.collection(collectionNames.RENTS)
			.find({
				'diapason.end': {
					$gte: new Date(),
				},
			})
			.sort({createdAt: -1})
			.toArray();

		return rentList.map((rent) => Rent.fromSnapshot(rent));
	}
}

module.exports = RentRepository;
