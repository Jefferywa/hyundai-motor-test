const collectionNames = require('../../../storage/mongo/collection_names');

const Car = require('../../../domains/car');

/**
 * @name Calendar.CarRepository
 */
class CarRepository {
	/**
	 * @param {Db} mongo
	 * @param {Logger} log
	 */
	constructor(mongo, log) {
		this._mongo = mongo;
		this._log = log;
	}

	/**
	 * @returns {Promise<Car[]>}
	 */
	async getAll() {
		this._log.info('TRY_GET_ALL_CARS');

		const carList = await this._mongo
			.collection(collectionNames.CARS)
			.find()
			.toArray();

		return carList.map((car) => Car.fromSnapshot(car));
	}
}

module.exports = CarRepository;
