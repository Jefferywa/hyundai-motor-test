const config = require('config');

const MongoClientFactory = require('./../../../storage/mongo/mongo_client_factory');

const {carsRegister} = require('./cars');

const {busyDaysRegister} = require('./busy_days');

exports.carsDependency = async (req, res, next) => {
	const {log} = req;

	const mongo = await MongoClientFactory.create(config, log);

	const container = carsRegister(req, mongo, log);
	const appCarsService = container.getInstance('appCarsService');

	res.locals = {
		...res.locals,
		appCarsService,
	};

	next();
};

exports.busyDaysDependency = async (req, res, next) => {
	const {log} = req;

	const mongo = await MongoClientFactory.create(config, log);

	const container = busyDaysRegister(req, mongo, log);
	const appBusyDaysService = container.getInstance('appBusyDaysService');

	res.locals = {
		...res.locals,
		appBusyDaysService,
	};

	next();
};
