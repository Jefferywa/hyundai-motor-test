const Container = require('true-ioc');

const AppCarsService = require('./../../../application/app_cars_service');

const CarRepository = require('./../../../infrastructure/repository/car_repository');

module.exports = () => {
	const container = new Container();

	// App service
	container.registerClass('appCarsService', AppCarsService);

	// Repository
	container.registerClass('carRepository', CarRepository);

	return container;
};
