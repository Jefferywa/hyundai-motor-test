const Container = require('true-ioc');

const register = require('./register');

const carsContainer = register();

/**
 * @param req
 * @param log
 * @param mongo
 * @returns {Container}
 */
exports.carsRegister = (req, mongo, log) => {
	const perRequestContainer = new Container(carsContainer);

	perRequestContainer.registerValue('req', req);
	perRequestContainer.registerValue('mongo', mongo);
	perRequestContainer.registerValue('log', log);

	return perRequestContainer;
};
