const Container = require('true-ioc');

const register = require('./register');

const busyDaysContainer = register();

/**
 * @param req
 * @param log
 * @param mongo
 * @returns {Container}
 */
exports.busyDaysRegister = (req, mongo, log) => {
	const perRequestContainer = new Container(busyDaysContainer);

	perRequestContainer.registerValue('req', req);
	perRequestContainer.registerValue('mongo', mongo);
	perRequestContainer.registerValue('log', log);

	return perRequestContainer;
};
