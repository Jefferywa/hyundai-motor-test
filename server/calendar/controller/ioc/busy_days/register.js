const Container = require('true-ioc');

const AppBusyDaysService = require('./../../../application/app_busy_days_service');

const RentRepository = require('./../../../infrastructure/repository/rent_repository');

module.exports = () => {
	const container = new Container();

	// App service
	container.registerClass('appBusyDaysService', AppBusyDaysService);

	// Repository
	container.registerClass('rentRepository', RentRepository);

	return container;
};
