const router = require('express').Router();
const asyncWrap = require('express-async-wrap');

const {carsDependency, busyDaysDependency} = require('./ioc');

router.get(
	'/cars',
	carsDependency,
	asyncWrap(async (req, res, next) => {
		res.result = await res.locals.appCarsService.execute(res.locals.appParams);

		next();
	})
);

router.get(
	'/busy-days',
	busyDaysDependency,
	asyncWrap(async (req, res, next) => {
		res.result = await res.locals.appBusyDaysService.execute(res.locals.appParams);

		next();
	})
);

module.exports = router;
