class Diapason {
	/**
	 * @param {Date} start
	 * @param {Date} end
	 */
	constructor(start, end) {
		this._start = start;
		this._end = end;

		this._snapshot = {
			start,
			end,
		};
	}

	/**
	 * @typedef Diapason.Snapshot
	 * @type {Object}
	 * @property {Date} start
	 * @property {Date} end
	 * @returns Diapason.Snapshot
	 */

	/**
	 * @return {Diapason.Snapshot}
	 */
	toJSON() {
		return this._snapshot;
	}

	/**
	 * @return {Diapason.Snapshot}
	 */
	toBSON() {
		return this._snapshot;
	}

	/**
	 * @param {Object|Diapason.Snapshot} snapshot
	 * @return {Diapason}
	 */
	static fromSnapshot(snapshot) {
		const start = new Date(snapshot.start);
		const end = new Date(snapshot.end);

		return new Diapason(start, end);
	}
}

module.exports = Diapason;
