const uuid = require('node-uuid').v4;

const Car = require('../car');
const Diapason = require('./diapason');

class Rent {
	/**
	 * @param {String} id
	 * @param {Car} car
	 * @param {Diapason} diapason
	 * @param {Date} createdAt
	 */
	constructor(id, car, diapason, createdAt) {
		this._id = id;
		this._car = car;
		this._diapason = diapason;
		this._createdAt = createdAt;

		this._snapshot = {
			id,
			car,
			diapason,
			createdAt,
		};
	}

	/**
	 * @param {Car} car
	 * @param {Diapason} diapason
	 * @returns {Rent}
	 */
	static create(car, diapason) {
		const id = uuid();
		const createdAt = new Date();

		return new Rent(id, car, diapason, createdAt);
	}

	/**
	 * @typedef Rent.Snapshot
	 * @type {Object}
	 * @property {String} id
	 * @property {Car} car
	 * @property {Diapason} diapason
	 * @property {Date} createdAt
	 * @returns Rent.Snapshot
	 */

	/**
	 * @returns {Rent.Snapshot}
	 */
	toJSON() {
		return this._snapshot;
	}

	/**
	 * @returns {Rent.Snapshot}
	 */
	toBSON() {
		return this._snapshot;
	}

	/**
	 * @param {Object|Rent.Snapshot} snapshot
	 * @returns {Rent}
	 */
	static fromSnapshot(snapshot) {
		const car = Car.fromSnapshot(snapshot.car);
		const diapason = Diapason.fromSnapshot(snapshot.diapason);
		const createdAt = new Date(snapshot.createdAt);

		return new Rent(snapshot.id, car, diapason, createdAt);
	}
}

module.exports = Rent;
