const {assert} = require('chai');
const moment = require('moment');

const Car = require('./../../car');
const Diapason = require('./../diapason');

const Rent = require('./../index');

describe('Domain Rent tests', () => {
	const carId = 1;
	const carModel = 'creta';
	const carLicense = 'А111АА750';

	const car = new Car(carId, carModel, carLicense);

	const startDate = moment().toDate();
	const endDate = moment()
		.add(1, 'days')
		.toDate();

	const diapason = new Diapason(startDate, endDate);

	it('Create Rent test', () => {
		const rent = Rent.create(car, diapason);

		const rentData = rent.toJSON();
		const rentCarData = rentData.car.toJSON();
		const rentDiapasonData = rentData.diapason.toJSON();

		assert.instanceOf(rent, Rent);
		assert.instanceOf(rentData.car, Car);
		assert.instanceOf(rentData.diapason, Diapason);

		assert.equal(rentCarData.id, carId);
		assert.equal(rentCarData.model, carModel);
		assert.equal(rentCarData.license, carLicense);

		assert.deepEqual(rentDiapasonData.start, startDate);
		assert.deepEqual(rentDiapasonData.end, endDate);
	});

	it('Create Rent from snapshot', () => {
		const rent = Rent.create(car, diapason);

		const rentStr = JSON.stringify(rent);
		const rentData = JSON.parse(rentStr);

		const reCreatedRent = Rent.fromSnapshot(rentData);

		const reCreatedRentData = reCreatedRent.toJSON();
		const reCreatedRentCarData = reCreatedRentData.car.toJSON();
		const reCreatedRentDiapasonData = reCreatedRentData.diapason.toJSON();

		assert.instanceOf(reCreatedRent, Rent);
		assert.instanceOf(reCreatedRentData.car, Car);
		assert.instanceOf(reCreatedRentData.diapason, Diapason);

		assert.equal(reCreatedRentCarData.id, carId);
		assert.equal(reCreatedRentCarData.model, carModel);
		assert.equal(reCreatedRentCarData.license, carLicense);

		assert.deepEqual(reCreatedRentDiapasonData.start, startDate);
		assert.deepEqual(reCreatedRentDiapasonData.end, endDate);
	});
});
