const {assert} = require('chai');
const moment = require('moment');

const Diapason = require('./../diapason');

describe('Domain Diapason tests', () => {
	const startDate = moment().toDate();
	const endDate = moment()
		.add(1, 'days')
		.toDate();

	it('Create Diapason test', () => {
		const diapason = new Diapason(startDate, endDate);

		const diapasonData = diapason.toJSON();

		assert.instanceOf(diapason, Diapason);
		assert.deepEqual(diapasonData.start, startDate);
		assert.deepEqual(diapasonData.end, endDate);
	});

	it('Create Diapason from snapshot', () => {
		const diapason = new Diapason(startDate, endDate);

		const diapasonStr = JSON.stringify(diapason);
		const diapasonData = JSON.parse(diapasonStr);

		const reCreatedDiapason = Diapason.fromSnapshot(diapasonData);
		const reCreatedDiapasonData = reCreatedDiapason.toJSON();

		assert.instanceOf(reCreatedDiapason, Diapason);
		assert.deepEqual(reCreatedDiapasonData.start, startDate);
		assert.deepEqual(reCreatedDiapasonData.end, endDate);
	});
});
