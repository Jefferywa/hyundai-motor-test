class Car {
	/**
	 * @param {Number} id
	 * @param {String} model
	 * @param {String} license
	 */
	constructor(id, model, license) {
		this._id = id;
		this._model = model;
		this._license = license;

		this._snapshot = {
			id,
			model,
			license,
		};
	}

	/**
	 * @typedef Car.Snapshot
	 * @type {Object}
	 * @property {Number} id
	 * @property {String} model
	 * @property {String} license
	 * @returns Car.Snapshot
	 */

	/**
	 * @returns {Car.Snapshot}
	 */
	toJSON() {
		return this._snapshot;
	}

	/**
	 * @returns {Car.Snapshot}
	 */
	toBSON() {
		return this._snapshot;
	}

	/**
	 * @param {Object|Car.Snapshot} snapshot
	 * @returns {Car}
	 */
	static fromSnapshot(snapshot) {
		return new Car(snapshot.id, snapshot.model, snapshot.license);
	}
}

module.exports = Car;
