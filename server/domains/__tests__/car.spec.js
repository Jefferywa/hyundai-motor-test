const {assert} = require('chai');

const Car = require('./../car');

describe('Domain Car tests', () => {
	const carId = 1;
	const carModel = 'creta';
	const carLicense = 'А111АА750';

	it('Create Car test', () => {
		const car = new Car(carId, carModel, carLicense);

		const carData = car.toJSON();

		assert.instanceOf(car, Car);
		assert.equal(carData.id, carId);
		assert.equal(carData.model, carModel);
		assert.equal(carData.license, carLicense);
	});

	it('Create Car from snapshot test', () => {
		const car = new Car(carId, carModel, carLicense);

		const carStr = JSON.stringify(car);
		const carData = JSON.parse(carStr);

		const reCreatedCar = Car.fromSnapshot(carData);
		const reCreatedCarData = reCreatedCar.toJSON();

		assert.instanceOf(reCreatedCar, Car);
		assert.equal(reCreatedCarData.id, carId);
		assert.equal(reCreatedCarData.model, carModel);
		assert.equal(reCreatedCarData.license, carLicense);
	});
});
