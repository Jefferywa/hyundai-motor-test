const config = require('config');
const _ = require('lodash');

module.exports = {
    level: config.logger.level,
    project: config.logger.project,
    name: config.logger.name,
    type: config.logger.type,
    writeMode: config.logger.write.mode,
    logPath: config.logger.write.path,
    useMapper: config.logger.useMapper,
    useTrimLogs: config.logger.useTrim,
    enableLevelZ: config.logger.enableLevelZ,
    maxMessageLength: config.logger.maxMessageLength,
    serializers: _.isEmpty(config.logger.serializers) ? {
        secure: function(privateData) {
            return mask(privateData);
        },
        dataBusMessage: function(data) {
            const dataStr = JSON.stringify(data);
            return mask(dataStr);
        },
        err: function(err) {
            return {
                message: JSON.stringify(err.message),
                name: err.name,
                stack: err.stack
            };
        },
        secureError: function({err, params, from}){
            const messageStr = JSON.stringify(err.message);
            const paramsStr =  JSON.stringify(params);

            return {
                error: {
                    message: mask(messageStr),
                    name: JSON.stringify(err.name),
                    stack: JSON.stringify(err.stack)
                },
                from: JSON.stringify(from),
                params: mask(paramsStr),
            };
        },
        paymentData: function(data) {
            const dataStr = JSON.stringify(data);
            return mask(dataStr);
        },
        stringData: (data) => {
            return JSON.stringify(data);
        },
        stringDataSecure: (data) => {
            const dataStr = JSON.stringify(data);
            return mask(dataStr);
        },
        secureMongodbUri: (url) => {
            return url.replace(/(\/\/).+:.+(@)/, '$1***:***$2');
        }
    } : config.logger.serializers
};

function mask(str){
    if (!str){
        return;
    }

    let result = str.replace(/(\d{4})\d{4,24}(\d{4})/g, '$1********$2');
    result = result.replace(/("password":|"pass":).+?("|,)/, '$1"****"');
    return result.replace(/("\d{3,4}")/g, '"***"');
}
