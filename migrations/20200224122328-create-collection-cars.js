module.exports = {
	async up(db) {
		return db.createCollection('cars', {
			validator: {
				$jsonSchema: {
					bsonType: 'object',
					required: ['id', 'model', 'license'],
					properties: {
						id: {
							bsonType: 'number',
							description: "must be a number and is required"
						},
						model: {
							bsonType: 'string'
						},
						license: {
							bsonType: 'string'
						},
					},
				},
			},
			validationLevel: 'strict',
			validationAction: 'error',
		});
	},
	async down(db) {
		return db.collection('cars').drop();
	},
};
