module.exports = {
  async up(db) {
    await db.collection('rents').createIndex({'id': -1});
    await db.collection('rents').createIndex({'car.id': -1});
    await db.collection('rents').createIndex({'diapason.start': -1});
  },
  async down(db) {
    await db.collection('rents').dropIndex('id_-1');
    await db.collection('rents').dropIndex('car.id_-1');
    await db.collection('rents').dropIndex('diapason.start_-1');
  },
};
