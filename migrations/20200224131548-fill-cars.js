module.exports = {
	async up(db) {
		return db.collection('cars').insertMany([
			{
				id: 1,
				model: 'creta',
				license: 'A111AA750',
			},
			{
				id: 2,
				model: 'creta',
				license: 'A111AA751',
			},
			{
				id: 3,
				model: 'creta',
				license: 'A111AA752',
			},
		]);
	},

	async down(db) {
		return db.collection('cars').deleteMany({});
	},
};
