module.exports = {
	async up(db) {
		return db.createCollection('rents', {
			validator: {
				$jsonSchema: {
					bsonType: 'object',
					required: ['id', 'car', 'diapason', 'createdAt'],
					properties: {
						id: {
							bsonType: 'string',
						},
						car: {
							bsonType: 'object',
							required: ['id', 'model', 'license'],
							properties: {
								id: {
									bsonType: 'number',
								},
								model: {
									bsonType: 'string',
								},
								license: {
									bsonType: 'string',
								},
							},
						},
						diapason: {
							bsonType: 'object',
							required: ['start', 'end'],
							properties: {
								start: {
									bsonType: 'date',
								},
								end: {
									bsonType: 'date',
								},
							},
						},
						createdAt: {
							bsonType: 'date',
						},
					},
				},
			},
			validationLevel: 'strict',
			validationAction: 'error',
		});
	},
	async down(db) {
		return db.collection('rents').drop();
	},
};
