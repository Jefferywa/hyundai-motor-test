module.exports = {
	async up(db) {
	    return db.collection('cars').createIndex({'id': -1});
    },
	async down(db) {
		return db.collection('cars').dropIndex('id_-1');
	},
};
