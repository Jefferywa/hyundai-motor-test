const path = require('path');
const fs = require('fs');

const DEFAULT_WRITE_MODE = 'STDOUT';
const DEFAULT_LOG_PATH = path.join(__dirname, `../../logs/log_${process.pid}.log`);

class Stream {
	constructor(meta, writeMode, logPath) {
		this._meta = meta;
		this._writeMode = writeMode || DEFAULT_WRITE_MODE;
		this._logPath = logPath || DEFAULT_LOG_PATH;
		this._levels = {
			70: 'Z',
			60: 'C',
			50: 'E',
			40: 'W',
			30: 'I',
			20: 'D',
			10: 'T',
		};
	}

	_map(rec) {
		const {
			pid,
			className,
			msg,
			hostname,
			env,
			role,
			type,
			zone,
			name,
			time,
			level,
			v,
			__meta,
			...rest
		} = rec;

		let data;
		if (Object.keys(rest).length) {
			data = rest;
		}

		return {
			'@timestamp': time,
			env,
			source_host: hostname,
			name,
			role,
			type,
			zone,
			fields: {
				pid,
				type: this._levels[level],
				name: className,
			},
			message: msg,
			data,
			...__meta,
			...this._meta.get('log-meta'),
		};
	}

	write(rec) {
		if (this._writeMode !== DEFAULT_WRITE_MODE) {
			process.stdout.write(`${JSON.stringify(this._map(rec))}\n`);
		} else {
			const writeStream = fs.createWriteStream(this._logPath, {
				flags: 'a',
			});

			writeStream.write(`${JSON.stringify(this._map(rec))}\n`);
		}
	}
}

module.exports = Stream;
