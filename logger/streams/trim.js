const Stream = require('./stream');

class TrimStream extends Stream {
	constructor(meta, maxMessageLength) {
		super(meta);

		this._maxMessageLength = maxMessageLength || 1024;
	}

	_map(rec) {
		let {data, message, fields, ...rest} = super._map(rec);

		if (message.length > this._maxMessageLength) {
			message = message.slice(0, this._maxMessageLength).concat('...');
		}

		if (fields.type === this._levels[70]) {
			return {data, fields, message, ...rest};
		}

		if (fields.type === this._levels[50] && data && Object.keys(data).length === 1) {
			return {data, fields, message, ...rest};
		}

		return {
			fields,
			message,
			...rest,
		};
	}
}

module.exports = TrimStream;
