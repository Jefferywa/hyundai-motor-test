class DefaultStream {
	constructor(meta) {
		this._meta = meta;
		this._levels = {
			70: 'Z',
			60: 'C',
			50: 'E',
			40: 'W',
			30: 'I',
			20: 'D',
			10: 'T',
		};
	}

	_map(rec) {
		const {__meta, ...rest} = rec;

		return {
			'@timestamp': new Date(),
			...rest,
			levelType: this._levels[rec.level],
			...__meta,
			...this._meta.get('log-meta'),
		};
	}

	write(rec) {
		process.stdout.write(`${JSON.stringify(this._map(rec))}\n`);
	}
}

module.exports = DefaultStream;
