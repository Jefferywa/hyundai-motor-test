const als = require('async-local-storage');
const uuid = require('node-uuid').v4;
const Bunyan = require('bunyan');

const Timer = require('./timer');
const Stream = require('./streams/stream');
const TrimStream = require('./streams/trim');
const DefaultStream = require('./streams/default');

const TYPE = 'example';
const CHEF_ENV = 'local';

function headersSerializer(rawHeaders) {
	const headers = {...rawHeaders};

	if (headers.cookie) {
		headers.cookie = headers.cookie
			.replace(/(sid=).+?(;|$)/g, '$1***$2')
			.replace(/(rm=).+?(;|$)/g, '$1***$2');
	}

	if (headers.authorization) {
		headers.authorization = '***';
	}

	return headers;
}

function reqSerializer(req) {
	return {
		url: req.url,
		method: req.method,
		headers: headersSerializer(req.headers),
	};
}

function errSerializer(err) {
	return {
		name: err.name,
		message: JSON.stringify(err.message),
		stack: err.stack,
	};
}

let serializers = {
	req: reqSerializer,
	err: errSerializer,
};

const metaSub = {
	get: (key) => {
		// nop
	},
	set: (key, value) => {
		// nop
	},
};

class Logger extends Bunyan {
	constructor(params, options) {
		super(Logger._initLogger(params), options);

		if (params instanceof Logger) {
			this._params = params._params;
		} else {
			this._params = params;
		}

		this._meta = Logger._getMeta(this._params.useAls);
	}

	static create(params) {
		const log = new Logger(params);
		const project = params.project || params.name || TYPE;

		log.middleware = (req, res, next) => {
			const requestId = req.headers['x-request-id'] || uuid();
			const meta = {requestId};

			log._setLogMeta(meta);
			req.requestId = requestId;
			req.log = log.child({__meta: meta, className: 'server'});
			req.log.info({req}, `${project}_INCOMING_REQUEST`);

			if (params.useTrimLogs) {
				req.log.json({req}, `${project}_INCOMING_REQUEST`);
			}

			next();
		};

		return log;
	}

	getLogPath(req) {
		return [req.method, req.path.replace(/\//g, '_').trim()].join('');
	}

	json(arg, ...rest) {
		if (!this._params.enableLevelZ) {
			return;
		}

		let newArgs;

		if (typeof arg === 'string') {
			newArgs = [{level: 70}, arg];
		}
		if (typeof arg !== 'string') {
			newArgs = [{...arg, level: 70}];
		}

		const args = newArgs.concat(rest);

		this.info(...args);
	}

	log(arg, ...rest) {
		let args = {...rest};

		if (Array.isArray(rest)) {
			const key = rest[0];
			const value = rest[1];

			args = {[key]: value};
		}

		this.json({stringData: args}, arg);
	}

	/**
	 * @param useAls
	 * @returns {Object}
	 * @private
	 */
	static _getMeta(useAls) {
		if (useAls) {
			als.enable();
			return als;
		}

		return metaSub;
	}

	/**
	 * @param params
	 * @returns {Object}
	 * @private
	 */
	static _initLogger(params) {
		if (params instanceof this) {
			return params;
		}

		const meta = this._getMeta(params.useAls);
		const streams = [];
		const level = params.level || 'INFO';

		if (params.useMapper === undefined) {
			params.useMapper = true;
		}
		if (params.serializers) {
			serializers = Object.assign(serializers, params.serializers);
		}
		if (params.useMapper) {
			streams.push({
				type: 'raw',
				level,
				stream: this._createStream(params, meta),
			});
		} else if (params.streams && params.streams.length > 0) {
			streams.push(...params.streams);
		} else {
			streams.push({
				type: 'raw',
				level,
				stream: new DefaultStream(meta),
			});
		}

		return {
			env: process.env.SITE_MODE || CHEF_ENV,
			// role: process.env.CHEF_ROLE || CHEF_ENV,
			name: params.name || TYPE,
			type: params.type || TYPE,
			streams,
			serializers,
		};
	}

	/**
	 * @param meta
	 * @returns {Boolean}
	 */
	_setLogMeta(meta) {
		return this._meta.set('log-meta', meta);
	}

	/**
	 * @param params
	 * @param meta
	 * @returns {TrimStream|Stream}
	 * @private
	 */
	static _createStream(params, meta) {
		if (params.useTrimLogs) {
			return new TrimStream(meta, params.maxMessageLength);
		}

		return new Stream(meta, params.writeMode, params.logPath);
	}
}

module.exports = {
	Timer: Timer,
	Logger: Logger,
	asyncLocalStorage: als,
};
